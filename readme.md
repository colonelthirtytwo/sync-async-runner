A not-actually-async async runner (for coroutines, etc).

Lets you run futures in the current thread, blocking until they await or exit.

Primairly intended as a coroutine alternative, when you want a separate thread of executiong
but want to run it synchronously with your main thread. Can also be used for testing, and also
functions as a basic example of how to execute futures, as a stepping stone to writing your
own scheduler.

It's not very big, and the code has comments. Check the tests for some simple examples.

```rust
use sync_async_runner::runner;
use std::task::Poll;
use futures_channel::{
	mpsc,
	oneshot,
};
use futures_util::{
	pin_mut,
	stream::StreamExt,
};
//!
let (mut sender, receiver) = mpsc::channel(5);
sender.try_send(1u32).unwrap();
sender.try_send(2).unwrap();
sender.try_send(3).unwrap();
sender.close_channel();
let coro = runner(async move {
	pin_mut!(receiver);
	assert_eq!(receiver.next().await, Some(1));
	assert_eq!(receiver.next().await, Some(2));
	assert_eq!(receiver.next().await, Some(3));
	assert_eq!(receiver.next().await, None);
	42
});
pin_mut!(coro);
// Ready immediately since the messages are all there
assert_eq!(coro.as_mut().poll(), Poll::Ready(42));

```